#! /usr/bin/env Python3
from collections import namedtuple
from datetime import date
import time
import mysql.connector

db = mysql.connector.connect(host="127.0.0.1", user="python", passwd="hola123,", database="mineria")
cursor = db.cursor()
print(cursor.execute("show tables"))

def time_log():
    return str(time.strftime("%d %b %Y %H:%M:%S"))


def dumpdata(header, items, trailer):
    sum = 0
    if len(items) == int(trailer.items):
        for i in items:
            sum += float(i.neto)
        print(sum)
        if sum != trailer.total:
            log.write(time_log() +
                    "Error al leer el registro :" + header.id + " La suma de los items no corresponde" +
                    "con el total\n")
            total._replace(total=sum)
            return True
        else:
            log.write(time_log +
                    "Registro: " + registro.id + " Exitoso")
            return True

Cliente = namedtuple("Cliente", "id, num_fact, c_year, c_month, c_day, exc")
Item = namedtuple("Item", "ID, a_month, a_day , cant, neto")
items = []
trailer = namedtuple("Trailer", "items,total")

a = date.today()
data = open("MINERIA.TXT", "r")
log = open("ETL_" + str(date.today()) + ".txt", "w")
ValidHeader = False

for line in data:

    if line[0] == "H":
        registro = Cliente(id=line[1:9],
                           num_fact=line[9:15],
                           c_year=line[15:19], c_month=line[19:21], c_day=line[21:23],
                           exc=line[23:26])
        ValidHeader = True

    elif ValidHeader:
        if line[0] == "I":
            items.append(Item(ID=line.split()[0][1:6], a_day=line.split()[0][6:8], a_month=line.split()[1],
                              cant=line.split()[2], neto=line.split()[3]))
        elif line[0] == "T":
            total = trailer(items=line.split()[1], total=line.split()[2])
            dumpdata(registro, items,  total)
            ValidHeader = False
            items = []

        else:
            log.write(time_log() +
                "Error al leer el archivo en la linea " + str(data.tell()) + 
                " :" + line + " Se encontro un elemento header pero el header anterior no se finalizo\n")
    else:
        log.write(time_log() +
            "Error al leer el archivo en la linea " +str(data.tell()) + 
            " :" + line + " Error en el formato de entrada\n")

data.close()
log.close()